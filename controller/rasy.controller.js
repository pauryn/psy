const {pool} = require('../database/db.js');
const { Request } = require('tedious');
const fs = require('fs');
let rawdata = fs.readFileSync('./query/queries.json');
let queries = JSON.parse(rawdata);
let TYPES = require('tedious').TYPES;

class MainController {
    
    

    getAllRasy(req, res) {
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            const request = new Request(queries.getAllRasy, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc', function(rowCount, more, rows){
                res.send(rows);
            });
            
            connection.execSql(request);
            
        });
    }
    
    getOneRasy(req , res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getOneRasy, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            request.addParameter('id', TYPES.Int, req.params.id)
            request.on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getAllZdjecia(req , res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }            
            let request = new Request(queries.getAllZdjecia, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getZdjecia(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getZdjecia, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            request.addParameter('id', TYPES.Int, req.params.id)
            request.on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
    
        });
    }

    getOneCharakter(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getOneCharakter, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            request.addParameter('id', TYPES.Int, req.params.id)
            request.on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getOneRozmiar(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getOneRozmiar, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            request.addParameter('id', TYPES.Int, req.params.id)
            request.on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getOneSiersc(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getOneSiersc, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            request.addParameter('id', TYPES.Int, req.params.id)
            request.on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getOneUmaszczenie(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getOneUmaszczenie, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            request.addParameter('id', TYPES.Int, req.params.id)
            request.on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })

            connection.execSql(request);
        });
    }

    getAllCharakter(req , res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getAllCharakter, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getAllRozmiar(req , res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getAllRozmiar, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getAllSiersc(req , res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getAllSiersc, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            request.on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getAllUmaszczenie(req , res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getAllUmaszczenie, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request)
        });
    }

    getFilterCharakter(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getFilterCharakter, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getFilterRozmiar(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            let request = new Request(queries.getFilterRozmiar, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getFilterSiersc(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            const request = new Request(queries.getFilterSiersc, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

    getFilterUmaszczenie(req, res){
        pool.acquire(function (err, connection) {
            if (err) {
                console.error(err);
                return;
            }
            const request = new Request(queries.getFilterUmaszczenie, (err, rowCount, rows) => {
                if(err) {
                    res.status(500)
                    res.send(err.message)
                }
                else {
                    connection.release();
                }
            })
            .on('doneInProc',function(rowCount, more, rows){
                res.send(rows);
            })
            connection.execSql(request);
        });
    }

}

const controller = new MainController()
module.exports = controller;