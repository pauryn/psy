const ConnectionPool = require('tedious-connection-pool');

const config = {
  userName: 'pauryn',
  password: 'Xyz@1234',
  server: 'metajezyki.database.windows.net',
  options: {
    database: 'metajezyki',
    encrypt: true,
    rowCollectionOnDone: true
  }
};

var poolConfig = {
  min: 1,
  max: 10000,
  log: true
};

const pool = new ConnectionPool(poolConfig, config);

pool.acquire(function (err, connection) {
  if (err) {
      console.error('Database Connection Failed! Bad Config: ', err);
      return;
  }
  else {
      console.log('Connected to MSSQL');
  }
});

module.exports = {  
    pool
}  