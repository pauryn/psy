const express = require('express');
const cors = require('cors');
const path = require('path');
const router = require('./routes/rasy.routes');
const app = express();
  
app.use(cors());
app.use(express.urlencoded({ extended: false }));
app.use(express.json());

app.use(express.static(path.join(__dirname, 'dist')));

app.use('/api', router);


app.get('*', (req, res) => {
    res.sendFile(path.join(__dirname, 'dist/index.html'));
  });

const port = process.env.PORT || 8000;
  
app.listen(port, () => {
    console.log('Listening on port ' + port)
})


