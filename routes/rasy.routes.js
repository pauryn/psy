const express =  require('express');
const controller = require('../controller/rasy.controller')

const rasyRouter = express.Router();
rasyRouter.get('/all-breeds', controller.getAllRasy);
rasyRouter.get('/dog-details/:id', controller.getOneRasy);
rasyRouter.get('/photos', controller.getAllZdjecia);
rasyRouter.get('/photos/:id', controller.getZdjecia);
rasyRouter.get('/dog-details/size/:id',controller.getOneRozmiar);
rasyRouter.get('/size',controller.getAllRozmiar);
rasyRouter.get('/dog-details/fur/:id',controller.getOneSiersc);
rasyRouter.get('/fur',controller.getAllSiersc);
rasyRouter.get('/dog-details/character/:id',controller.getOneCharakter);
rasyRouter.get('/character',controller.getAllCharakter);
rasyRouter.get('/dog-details/color/:id',controller.getOneUmaszczenie);
rasyRouter.get('/color',controller.getAllUmaszczenie);
rasyRouter.get('/filter/size',controller.getFilterRozmiar);
rasyRouter.get('/filter/fur',controller.getFilterSiersc);
rasyRouter.get('/filter/character',controller.getFilterCharakter);
rasyRouter.get('/filter/color',controller.getFilterUmaszczenie);



module.exports = rasyRouter;