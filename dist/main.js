(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "+JMX":
/*!*****************************************!*\
  !*** ./src/app/service/crud.service.ts ***!
  \*****************************************/
/*! exports provided: CrudService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "CrudService", function() { return CrudService; });
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! rxjs/operators */ "kU1M");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "qCKp");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");





class CrudService {
    constructor(httpClient) {
        this.httpClient = httpClient;
        // Node/Express API
        this.REST_API = 'https://afternoon-everglades-89596.herokuapp.com/api';
        // Http Header
        this.httpHeaders = new _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpHeaders"]().set('Content-Type', 'application/json');
    }
    GetRasy() {
        return this.httpClient.get(`${this.REST_API}/all-breeds`);
    }
    GetOneRasy(id) {
        let API_URL = `${this.REST_API}/dog-details/${id}`;
        return this.httpClient.get(API_URL, { headers: this.httpHeaders })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])((res) => {
            return res || {};
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
    }
    GetZdjecia() {
        return this.httpClient.get(`${this.REST_API}/photos`);
    }
    GetOneZdjecia(id) {
        let API_URL = `${this.REST_API}/photos/${id}`;
        return this.httpClient.get(API_URL, { headers: this.httpHeaders })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])((res) => {
            return res || {};
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
    }
    GetOneRozmiar(id) {
        let API_URL = `${this.REST_API}/dog-details/size/${id}`;
        return this.httpClient.get(API_URL, { headers: this.httpHeaders })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])((res) => {
            return res || {};
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
    }
    GetAllRozmiar() {
        return this.httpClient.get(`${this.REST_API}/size`);
    }
    GetOneSiersc(id) {
        let API_URL = `${this.REST_API}/dog-details/fur/${id}`;
        return this.httpClient.get(API_URL, { headers: this.httpHeaders })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])((res) => {
            return res || {};
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
    }
    GetAllSiersc() {
        return this.httpClient.get(`${this.REST_API}/fur `);
    }
    GetOneCharakter(id) {
        let API_URL = `${this.REST_API}/dog-details/character/${id}`;
        return this.httpClient.get(API_URL, { headers: this.httpHeaders })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])((res) => {
            return res || {};
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
    }
    GetAllCharakter() {
        return this.httpClient.get(`${this.REST_API}/character`);
    }
    GetOneUmaszczenie(id) {
        let API_URL = `${this.REST_API}/dog-details/color/${id}`;
        return this.httpClient.get(API_URL, { headers: this.httpHeaders })
            .pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["map"])((res) => {
            return res || {};
        }), Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_0__["catchError"])(this.handleError));
    }
    GetAllUmaszczenie() {
        return this.httpClient.get(`${this.REST_API}/color`);
    }
    GetFilterRozmiar() {
        return this.httpClient.get(`${this.REST_API}/filter/size`);
    }
    GetFilterCharakter() {
        return this.httpClient.get(`${this.REST_API}/filter/character`);
    }
    GetFilterUmaszczenie() {
        return this.httpClient.get(`${this.REST_API}/filter/color`);
    }
    GetFilterSiersc() {
        return this.httpClient.get(`${this.REST_API}/filter/fur`);
    }
    // Error 
    handleError(error) {
        let errorMessage = '';
        if (error.error instanceof ErrorEvent) {
            errorMessage = error.error.message;
        }
        else {
            errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        }
        console.log(errorMessage);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_1__["throwError"])(errorMessage);
    }
}
CrudService.ɵfac = function CrudService_Factory(t) { return new (t || CrudService)(_angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵinject"](_angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"])); };
CrudService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjectable"]({ token: CrudService, factory: CrudService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! E:\Metajezyki\Psy\src\main.ts */"zUnb");


/***/ }),

/***/ "1xP8":
/*!****************************************************!*\
  !*** ./src/app/dog-detail/dog-detail.component.ts ***!
  \****************************************************/
/*! exports provided: DogDetailComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "DogDetailComponent", function() { return DogDetailComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _service_crud_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/crud.service */ "+JMX");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");






const _c0 = ["photos"];
const _c1 = ["info"];
const _c2 = ["appearance"];
const _c3 = ["character"];
function DogDetailComponent_ng_container_0_17_ng_template_0_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 14);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](1, "img", 15);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element2_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", element2_r11[1].value, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function DogDetailComponent_ng_container_0_17_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, DogDetailComponent_ng_container_0_17_ng_template_0_Template, 2, 1, "ng-template", 13);
} }
function DogDetailComponent_ng_container_0_li_23_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element3_r14 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element3_r14[1].value);
} }
function DogDetailComponent_ng_container_0_li_27_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element4_r15 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element4_r15[1].value);
} }
function DogDetailComponent_ng_container_0_li_31_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element5_r16 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element5_r16[1].value);
} }
function DogDetailComponent_ng_container_0_li_35_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "li");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element6_r17 = ctx.$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element6_r17[1].value);
} }
function DogDetailComponent_ng_container_0_Template(rf, ctx) { if (rf & 1) {
    const _r19 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "li", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DogDetailComponent_ng_container_0_Template_li_click_5_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r18 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r18.scrollPhotos(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Zdj\u0119cia");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "li", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DogDetailComponent_ng_container_0_Template_li_click_7_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r20 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r20.scrollInfo(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8, "Podstawowe informacje");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "li", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DogDetailComponent_ng_container_0_Template_li_click_9_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r21 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r21.scrollAppearance(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Wygl\u0105d");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](11, "li", 2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function DogDetailComponent_ng_container_0_Template_li_click_11_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r19); const ctx_r22 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r22.scrollCharacter(); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](12, "Charakter");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "div", 3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](14, "div", 4, 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "ngb-carousel", 6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](17, DogDetailComponent_ng_container_0_17_Template, 1, 0, undefined, 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](18, "div", 7, 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](21, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](22, "Rozmiar");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](23, DogDetailComponent_ng_container_0_li_23_Template, 2, 1, "li", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](24, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](25, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](26, "Sier\u015B\u0107");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](27, DogDetailComponent_ng_container_0_li_27_Template, 2, 1, "li", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](28, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](29, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](30, "Charakter");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](31, DogDetailComponent_ng_container_0_li_31_Template, 2, 1, "li", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](32, "ul");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](33, "h3");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](34, "Umaszczenie");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](35, DogDetailComponent_ng_container_0_li_35_Template, 2, 1, "li", 0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](36, "div", 9, 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](38, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](39, "Wygl\u0105d");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](40);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](41, "div", 11, 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](43, "h2");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](44, "Charakter");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](45);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const element_r1 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r1[1].value);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r0.active == 1 ? "active" : "unactive");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r0.active == 2 ? "active" : "unactive");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r0.active == 3 ? "active" : "unactive");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r0.active == 4 ? "active" : "unactive");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("interval", 8000);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.Zdjecia);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](6);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.Rozmiar);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.Siersc);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.Charakter);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r0.Umaszczenie);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element_r1[2].value, " ");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate1"](" ", element_r1[3].value, " ");
} }
class DogDetailComponent {
    constructor(activatedRoute, crudService, sanitizer) {
        this.activatedRoute = activatedRoute;
        this.crudService = crudService;
        this.sanitizer = sanitizer;
        this.active = '1';
        this.areas = ['1', '2', '3', '4'];
    }
    ngOnInit() {
        this.getId = this.activatedRoute.snapshot.paramMap.get('id');
        this.crudService.GetOneRasy(this.getId).subscribe(res => {
            this.Rasy = res;
        });
        this.crudService.GetOneZdjecia(this.getId).subscribe(res => {
            this.Zdjecia = res;
            for (var i = 0; i < this.Zdjecia.length; i++) {
                var binary = '';
                var bytes = new Uint8Array(this.Zdjecia[i][1].value.data);
                var len = bytes.byteLength;
                for (var j = 0; j < len; j++) {
                    binary += String.fromCharCode(bytes[j]);
                }
                var btoaVal = btoa(binary);
                this.Zdjecia[i][1].value = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + btoaVal);
            }
        });
        this.crudService.GetOneRozmiar(this.getId).subscribe(res => {
            this.Rozmiar = res;
        });
        this.crudService.GetOneSiersc(this.getId).subscribe(res => {
            this.Siersc = res;
        });
        this.crudService.GetOneCharakter(this.getId).subscribe(res => {
            this.Charakter = res;
        });
        this.crudService.GetOneUmaszczenie(this.getId).subscribe(res => {
            this.Umaszczenie = res;
        });
    }
    scrollPhotos() {
        this.photos.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'end' });
    }
    scrollInfo() {
        this.info.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
    scrollAppearance() {
        this.appearance.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
    scrollCharacter() {
        this.character.nativeElement.scrollIntoView({ behavior: 'smooth', block: 'start' });
    }
    onScroll(event) {
        this.active = this.areas[Math.round(window.pageYOffset / document.documentElement.clientHeight)];
    }
}
DogDetailComponent.ɵfac = function DogDetailComponent_Factory(t) { return new (t || DogDetailComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_router__WEBPACK_IMPORTED_MODULE_1__["ActivatedRoute"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_crud_service__WEBPACK_IMPORTED_MODULE_2__["CrudService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_3__["DomSanitizer"])); };
DogDetailComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: DogDetailComponent, selectors: [["app-dog-detail"]], viewQuery: function DogDetailComponent_Query(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c0, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c1, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c2, 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵviewQuery"](_c3, 1);
    } if (rf & 2) {
        let _t;
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.photos = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.info = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.appearance = _t.first);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵqueryRefresh"](_t = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵloadQuery"]()) && (ctx.character = _t.first);
    } }, hostBindings: function DogDetailComponent_HostBindings(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("scroll", function DogDetailComponent_scroll_HostBindingHandler($event) { return ctx.onScroll($event); }, false, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵresolveWindow"]);
    } }, decls: 1, vars: 1, consts: [[4, "ngFor", "ngForOf"], ["id", "menu"], [3, "ngClass", "click"], ["id", "container"], ["id", "photos", 1, "elements"], ["photos", ""], [3, "interval"], ["id", "info", 1, "elements"], ["info", ""], ["id", "appearance", 1, "elements"], ["appearance", ""], ["id", "character", 1, "elements"], ["character", ""], ["ngbSlide", ""], [1, "picsum-img-wrapper"], [3, "src"]], template: function DogDetailComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](0, DogDetailComponent_ng_container_0_Template, 46, 13, "ng-container", 0);
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.Rasy);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgClass"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbCarousel"], _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_5__["NgbSlide"]], styles: ["#menu[_ngcontent-%COMP%] {\n  margin: 5%;\n  text-align: center;\n}\n\nul[_ngcontent-%COMP%] {\n  display: block;\n  margin: 1rem 0;\n  padding: 0;\n}\n\nli[_ngcontent-%COMP%] {\n  display: inline-block;\n  list-style: none;\n  font-weight: 600;\n  padding: 0.3rem 0.7rem;\n  margin: 0.3rem;\n  text-align: center;\n}\n\n.unactive[_ngcontent-%COMP%], li[_ngcontent-%COMP%] {\n  border: 1px solid #555555;\n  color: #555555;\n}\n\nli[_ngcontent-%COMP%]:hover, .active[_ngcontent-%COMP%] {\n  background-color: #cda398;\n  border: #cda398 1px solid;\n}\n\n.elements[_ngcontent-%COMP%] {\n  min-height: 50vh;\n  padding: 15vh 2rem 2rem 2rem;\n  font-size: 1rem;\n  text-align: justify;\n  display: flex;\n  justify-content: center;\n  align-items: center;\n  flex-direction: column;\n  width: 100%;\n}\n\n.elements[_ngcontent-%COMP%]   h2[_ngcontent-%COMP%] {\n  margin-bottom: 4rem;\n}\n\n#info[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n#info[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n  min-width: 15%;\n}\n\n#photos[_ngcontent-%COMP%] {\n  padding: 0;\n  min-height: 40vh;\n  display: block;\n}\n\nimg[_ngcontent-%COMP%] {\n  display: block;\n  margin: auto;\n  object-fit: cover;\n  width: 100%;\n  min-height: 100%;\n}\n\n@media (min-width: 768px) {\n  #container[_ngcontent-%COMP%] {\n    position: relative;\n    margin-left: 20%;\n  }\n\n  #menu[_ngcontent-%COMP%] {\n    position: fixed;\n    left: 0;\n    width: 20%;\n    height: 100vh;\n    overflow-y: auto;\n    background-color: #f2f2f2;\n    margin: 0;\n    padding: 1rem 1rem 10rem 1rem;\n  }\n\n  #menu[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n    font-size: 1.75rem;\n    font-weight: 900;\n    margin: 20% 0;\n  }\n\n  #menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n    margin: 1rem 0;\n    color: #555555;\n    width: 90%;\n    text-align: center;\n    font-size: 1rem;\n  }\n\n  .elements[_ngcontent-%COMP%] {\n    min-height: 100vh;\n    font-size: 1.25rem;\n    padding: 15vh 4rem 4rem 4rem;\n  }\n}\n\n@media (min-width: 1100px) {\n  #container[_ngcontent-%COMP%] {\n    margin-left: 20%;\n  }\n\n  #menu[_ngcontent-%COMP%] {\n    width: 20%;\n  }\n\n  #menu[_ngcontent-%COMP%]   li[_ngcontent-%COMP%] {\n    font-size: 1.5rem;\n  }\n\n  #menu[_ngcontent-%COMP%]   h3[_ngcontent-%COMP%] {\n    font-size: 2.5rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGRvZy1kZXRhaWwuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7RUFDQSxjQUFBO0VBQ0EsVUFBQTtBQUNKOztBQUVBO0VBQ0kscUJBQUE7RUFDQSxnQkFBQTtFQUNBLGdCQUFBO0VBQ0Esc0JBQUE7RUFDQSxjQUFBO0VBQ0Esa0JBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0VBQ0EsY0FBQTtBQUNKOztBQUVBO0VBQ0kseUJBQUE7RUFDQSx5QkFBQTtBQUNKOztBQUdBO0VBQ0ksZ0JBQUE7RUFDQSw0QkFBQTtFQUNBLGVBQUE7RUFDQSxtQkFBQTtFQUNBLGFBQUE7RUFDQSx1QkFBQTtFQUNBLG1CQUFBO0VBQ0Esc0JBQUE7RUFDQSxXQUFBO0FBQUo7O0FBR0E7RUFDSSxtQkFBQTtBQUFKOztBQUdBO0VBQ0ksa0JBQUE7QUFBSjs7QUFHQTtFQUNJLGNBQUE7QUFBSjs7QUFJQTtFQUNJLFVBQUE7RUFDQSxnQkFBQTtFQUNBLGNBQUE7QUFESjs7QUFJQTtFQUNJLGNBQUE7RUFDQSxZQUFBO0VBQ0EsaUJBQUE7RUFDQSxXQUFBO0VBQ0EsZ0JBQUE7QUFESjs7QUFLQTtFQUNJO0lBQ0ksa0JBQUE7SUFDQSxnQkFBQTtFQUZOOztFQUtFO0lBQ0ksZUFBQTtJQUNBLE9BQUE7SUFDQSxVQUFBO0lBQ0EsYUFBQTtJQUNBLGdCQUFBO0lBQ0EseUJBQUE7SUFDQSxTQUFBO0lBQ0EsNkJBQUE7RUFGTjs7RUFNRTtJQUNJLGtCQUFBO0lBQ0EsZ0JBQUE7SUFDQSxhQUFBO0VBSE47O0VBTUU7SUFDSSxjQUFBO0lBQ0EsY0FBQTtJQUNBLFVBQUE7SUFDQSxrQkFBQTtJQUNBLGVBQUE7RUFITjs7RUFNRTtJQUNJLGlCQUFBO0lBQ0Esa0JBQUE7SUFDQSw0QkFBQTtFQUhOO0FBQ0Y7O0FBUUE7RUFDSTtJQUNJLGdCQUFBO0VBTk47O0VBU0U7SUFDSSxVQUFBO0VBTk47O0VBU0U7SUFDSSxpQkFBQTtFQU5OOztFQVNFO0lBQ0ksaUJBQUE7RUFOTjtBQUNGIiwiZmlsZSI6ImRvZy1kZXRhaWwuY29tcG9uZW50LnNjc3MiLCJzb3VyY2VzQ29udGVudCI6WyIjbWVudXtcclxuICAgIG1hcmdpbjogNSU7XHJcbiAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbn1cclxuXHJcbnVsIHtcclxuICAgIGRpc3BsYXk6IGJsb2NrO1xyXG4gICAgbWFyZ2luOiAxcmVtIDA7XHJcbiAgICBwYWRkaW5nOiAwO1xyXG59XHJcblxyXG5saSB7XHJcbiAgICBkaXNwbGF5OmlubGluZS1ibG9jaztcclxuICAgIGxpc3Qtc3R5bGU6IG5vbmU7XHJcbiAgICBmb250LXdlaWdodDogNjAwO1xyXG4gICAgcGFkZGluZzogMC4zcmVtIDAuN3JlbTtcclxuICAgIG1hcmdpbjogMC4zcmVtO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4udW5hY3RpdmUsIGxpIHtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1NTU1NTU7XHJcbiAgICBjb2xvcjogIzU1NTU1NTtcclxufVxyXG5cclxubGk6aG92ZXIsIC5hY3RpdmV7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2RhMzk4O1xyXG4gICAgYm9yZGVyOiAjY2RhMzk4IDFweCBzb2xpZDtcclxufVxyXG5cclxuXHJcbi5lbGVtZW50cyB7XHJcbiAgICBtaW4taGVpZ2h0OiA1MHZoO1xyXG4gICAgcGFkZGluZzogMTV2aCAycmVtIDJyZW0gMnJlbTtcclxuICAgIGZvbnQtc2l6ZTogMXJlbTtcclxuICAgIHRleHQtYWxpZ246IGp1c3RpZnk7XHJcbiAgICBkaXNwbGF5OiBmbGV4O1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgZmxleC1kaXJlY3Rpb246IGNvbHVtbjtcclxuICAgIHdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uZWxlbWVudHMgaDIge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogNHJlbTtcclxufVxyXG5cclxuI2luZm8ge1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG4jaW5mbyBsaXtcclxuICAgIG1pbi13aWR0aDogMTUlO1xyXG59XHJcblxyXG5cclxuI3Bob3RvcyB7XHJcbiAgICBwYWRkaW5nOjA7XHJcbiAgICBtaW4taGVpZ2h0OiA0MHZoO1xyXG4gICAgZGlzcGxheTogYmxvY2s7XHJcbn1cclxuXHJcbmltZyB7XHJcbiAgICBkaXNwbGF5OiBibG9jaztcclxuICAgIG1hcmdpbjogYXV0bztcclxuICAgIG9iamVjdC1maXQ6IGNvdmVyO1xyXG4gICAgd2lkdGg6IDEwMCU7XHJcbiAgICBtaW4taGVpZ2h0OiAxMDAlO1xyXG59XHJcblxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XHJcbiAgICAjY29udGFpbmVye1xyXG4gICAgICAgIHBvc2l0aW9uOiByZWxhdGl2ZTtcclxuICAgICAgICBtYXJnaW4tbGVmdDogMjAlOyBcclxuICAgIH1cclxuXHJcbiAgICAjbWVudXtcclxuICAgICAgICBwb3NpdGlvbjogZml4ZWQ7XHJcbiAgICAgICAgbGVmdDogMDtcclxuICAgICAgICB3aWR0aDogMjAlO1xyXG4gICAgICAgIGhlaWdodDogMTAwdmg7XHJcbiAgICAgICAgb3ZlcmZsb3cteTogYXV0bztcclxuICAgICAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJmMmYyO1xyXG4gICAgICAgIG1hcmdpbjogMDtcclxuICAgICAgICBwYWRkaW5nOiAxcmVtIDFyZW0gMTByZW0gMXJlbTtcclxuICAgICAgIFxyXG4gICAgfVxyXG5cclxuICAgICNtZW51IGgze1xyXG4gICAgICAgIGZvbnQtc2l6ZTogMS43NXJlbTtcclxuICAgICAgICBmb250LXdlaWdodDogOTAwO1xyXG4gICAgICAgIG1hcmdpbjogMjAlIDA7XHJcbiAgICB9XHJcblxyXG4gICAgI21lbnUgbGl7XHJcbiAgICAgICAgbWFyZ2luOiAxcmVtIDA7XHJcbiAgICAgICAgY29sb3I6ICM1NTU1NTU7XHJcbiAgICAgICAgd2lkdGg6IDkwJTtcclxuICAgICAgICB0ZXh0LWFsaWduOiBjZW50ZXI7XHJcbiAgICAgICAgZm9udC1zaXplOiAxcmVtO1xyXG4gICAgfVxyXG5cclxuICAgIC5lbGVtZW50cyB7XHJcbiAgICAgICAgbWluLWhlaWdodDogMTAwdmg7XHJcbiAgICAgICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG4gICAgICAgIHBhZGRpbmc6IDE1dmggNHJlbSA0cmVtIDRyZW07XHJcbiAgICB9XHJcblxyXG4gICAgXHJcbiAgICBcclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDogMTEwMHB4KSB7XHJcbiAgICAjY29udGFpbmVye1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMCU7XHJcbiAgICB9XHJcblxyXG4gICAgI21lbnUge1xyXG4gICAgICAgIHdpZHRoOiAyMCU7XHJcbiAgICB9XHJcblxyXG4gICAgI21lbnUgbGl7XHJcbiAgICAgICAgZm9udC1zaXplOiAxLjVyZW07XHJcbiAgICB9XHJcblxyXG4gICAgI21lbnUgaDN7XHJcbiAgICAgICAgZm9udC1zaXplOiAyLjVyZW07XHJcbiAgICB9XHJcbiAgICBcclxufSJdfQ== */"] });


/***/ }),

/***/ "38m8":
/*!********************************************!*\
  !*** ./src/app/service/filters.service.ts ***!
  \********************************************/
/*! exports provided: FiltersService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltersService", function() { return FiltersService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");

class FiltersService {
    constructor() { }
    findDuplicates(arr) {
        let seen = [];
        let a = [];
        arr.forEach(function (str) {
            if (seen[str]) {
                a.push(str);
            }
            else
                seen[str] = true;
        });
        return a;
    }
    getAllId(arr, val) {
        let indexes = [];
        for (let i = 0; i < arr.length; i++) {
            if (arr[i].id == val)
                indexes.push(arr[i].rasa);
        }
        return indexes;
    }
    findInArray(selectedArr, arr) {
        let data = { selectedCount: 0, selected: false, filtered: [] };
        let array = [];
        let number = 0;
        for (let i = 0; i < selectedArr.length; i++) {
            if (selectedArr[i].selected == true) {
                data.selectedCount++;
                data.selected = true;
                let ids = this.getAllId(arr, selectedArr[i].id);
                ids.forEach(function (x) {
                    array.push(x);
                });
                if (number > 0) {
                    data.filtered = this.findDuplicates(array);
                    array = data.filtered;
                }
                else
                    data.filtered = array;
                number++;
            }
        }
        return data;
    }
    andFilters(rasy, filtered, selected) {
        if (rasy[0] != false) {
            if (filtered.length > 0) {
                if (rasy.length > 0) {
                    let a = filtered.concat(rasy);
                    rasy = this.findDuplicates(a);
                    if (rasy.length == 0)
                        rasy.push(false);
                }
                else
                    rasy = filtered;
            }
            else if (selected == true) {
                rasy = [];
                rasy.push(false);
            }
        }
        return rasy;
    }
}
FiltersService.ɵfac = function FiltersService_Factory(t) { return new (t || FiltersService)(); };
FiltersService.ɵprov = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineInjectable"]({ token: FiltersService, factory: FiltersService.ɵfac, providedIn: 'root' });


/***/ }),

/***/ "AytR":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "Sy1n":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "tyNb");


class AppComponent {
    constructor() {
        this.title = 'Psy';
    }
}
AppComponent.ɵfac = function AppComponent_Factory(t) { return new (t || AppComponent)(); };
AppComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AppComponent, selectors: [["app-root"]], decls: 8, vars: 0, consts: [["id", "main"], ["routerLink", "/breeds"]], template: function AppComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "header", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "h1");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Rasy Ps\u00F3w");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](4, "router-outlet");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "footer");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](7, "\u00A9 Paulina Ryngwelska 2021");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } }, directives: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterLink"], _angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterOutlet"]], styles: ["@import url(\"https://fonts.googleapis.com/css2?family=Indie+Flower&display=swap\");\n#main[_ngcontent-%COMP%] {\n  min-height: 100%;\n  display: flex;\n  flex-direction: column;\n}\nheader[_ngcontent-%COMP%], footer[_ngcontent-%COMP%] {\n  width: 100%;\n  font-family: \"Indie Flower\";\n  color: #CDA398;\n  background-color: #f2f2f2;\n  text-align: center;\n  padding: 1.5rem;\n}\nheader[_ngcontent-%COMP%] {\n  position: sticky;\n  z-index: 16;\n  border-bottom: solid 1px #d7d7d7;\n  top: 0;\n  min-height: 10vh;\n}\nfooter[_ngcontent-%COMP%] {\n  position: sticky;\n  z-index: 1;\n  border-top: solid 1px #d7d7d7;\n  margin-top: auto;\n}\nh1[_ngcontent-%COMP%], h3[_ngcontent-%COMP%] {\n  margin: 0;\n}\n@media (min-width: 768px) {\n  h1[_ngcontent-%COMP%] {\n    font-size: 3rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcYXBwLmNvbXBvbmVudC5zY3NzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQUFRLGlGQUFBO0FBR1I7RUFDSSxnQkFBQTtFQUNBLGFBQUE7RUFDQSxzQkFBQTtBQURKO0FBSUE7RUFDSSxXQUFBO0VBQ0EsMkJBQUE7RUFDQSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxrQkFBQTtFQUNBLGVBQUE7QUFESjtBQUdBO0VBQ0ksZ0JBQUE7RUFDQSxXQUFBO0VBQ0EsZ0NBQUE7RUFDQSxNQUFBO0VBQ0EsZ0JBQUE7QUFBSjtBQUdBO0VBQ0ksZ0JBQUE7RUFDQSxVQUFBO0VBQ0EsNkJBQUE7RUFDQSxnQkFBQTtBQUFKO0FBR0E7RUFDSSxTQUFBO0FBQUo7QUFFQTtFQUNJO0lBQUssZUFBQTtFQUVQO0FBQ0YiLCJmaWxlIjoiYXBwLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiQGltcG9ydCB1cmwoJ2h0dHBzOi8vZm9udHMuZ29vZ2xlYXBpcy5jb20vY3NzMj9mYW1pbHk9SW5kaWUrRmxvd2VyJmRpc3BsYXk9c3dhcCcpO1xyXG5cclxuXHJcbiNtYWlue1xyXG4gICAgbWluLWhlaWdodDogMTAwJTtcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG5oZWFkZXIsIGZvb3RlcntcclxuICAgIHdpZHRoOiAxMDAlO1xyXG4gICAgZm9udC1mYW1pbHk6ICdJbmRpZSBGbG93ZXInO1xyXG4gICAgY29sb3I6ICNDREEzOTg7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJmMmYyO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG4gICAgcGFkZGluZzogMS41cmVtO1xyXG59XHJcbmhlYWRlciB7XHJcbiAgICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gICAgei1pbmRleDogMTY7XHJcbiAgICBib3JkZXItYm90dG9tOiBzb2xpZCAxcHggI2Q3ZDdkNztcclxuICAgIHRvcDogMDtcclxuICAgIG1pbi1oZWlnaHQ6IDEwdmg7XHJcbn1cclxuXHJcbmZvb3RlciB7XHJcbiAgICBwb3NpdGlvbjogc3RpY2t5O1xyXG4gICAgei1pbmRleDogMTtcclxuICAgIGJvcmRlci10b3A6IHNvbGlkIDFweCAjZDdkN2Q3O1xyXG4gICAgbWFyZ2luLXRvcDogYXV0bztcclxuICAgIFxyXG59XHJcbmgxLCBoM3tcclxuICAgIG1hcmdpbjogMDtcclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDogNzY4cHgpIHtcclxuICAgIGgxIHsgZm9udC1zaXplOiAzcmVtO31cclxufSJdfQ== */"] });


/***/ }),

/***/ "ZAI4":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app-routing.module */ "vY5A");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app.component */ "Sy1n");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common/http */ "tk/3");
/* harmony import */ var _dog_detail_dog_detail_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./dog-detail/dog-detail.component */ "1xP8");
/* harmony import */ var _filters_filters_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./filters/filters.component */ "sH3A");
/* harmony import */ var _all_breeds_all_breeds_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./all-breeds/all-breeds.component */ "v69q");
/* harmony import */ var _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! @ng-bootstrap/ng-bootstrap */ "1kSV");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/core */ "fXoL");










class AppModule {
}
AppModule.ɵfac = function AppModule_Factory(t) { return new (t || AppModule)(); };
AppModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdefineNgModule"]({ type: AppModule, bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"]] });
AppModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵdefineInjector"]({ providers: [], imports: [[
            _angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
            _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
            _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"],
            _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
            _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"]
        ]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_9__["ɵɵsetNgModuleScope"](AppModule, { declarations: [_app_component__WEBPACK_IMPORTED_MODULE_2__["AppComponent"],
        _dog_detail_dog_detail_component__WEBPACK_IMPORTED_MODULE_4__["DogDetailComponent"],
        _filters_filters_component__WEBPACK_IMPORTED_MODULE_5__["FiltersComponent"],
        _all_breeds_all_breeds_component__WEBPACK_IMPORTED_MODULE_6__["AllBreedsComponent"]], imports: [_angular_common_http__WEBPACK_IMPORTED_MODULE_3__["HttpClientModule"],
        _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
        _app_routing_module__WEBPACK_IMPORTED_MODULE_1__["AppRoutingModule"],
        _ng_bootstrap_ng_bootstrap__WEBPACK_IMPORTED_MODULE_7__["NgbModule"],
        _angular_forms__WEBPACK_IMPORTED_MODULE_8__["FormsModule"]] }); })();


/***/ }),

/***/ "sH3A":
/*!**********************************************!*\
  !*** ./src/app/filters/filters.component.ts ***!
  \**********************************************/
/*! exports provided: FiltersComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FiltersComponent", function() { return FiltersComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _service_crud_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/crud.service */ "+JMX");
/* harmony import */ var _service_filters_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../service/filters.service */ "38m8");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/common */ "ofXK");





function FiltersComponent_button_7_Template(rf, ctx) { if (rf & 1) {
    const _r6 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FiltersComponent_button_7_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r6); const element_r4 = ctx.$implicit; const ctx_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r5.select("rozmiar", element_r4[0].value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element_r4 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r0.selectClass("rozmiar", element_r4[0].value));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r4[1].value);
} }
function FiltersComponent_button_11_Template(rf, ctx) { if (rf & 1) {
    const _r9 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FiltersComponent_button_11_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r9); const element_r7 = ctx.$implicit; const ctx_r8 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r8.select("charakter", element_r7[0].value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element_r7 = ctx.$implicit;
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r1.selectClass("charakter", element_r7[0].value));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r7[1].value);
} }
function FiltersComponent_button_15_Template(rf, ctx) { if (rf & 1) {
    const _r12 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FiltersComponent_button_15_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r12); const element_r10 = ctx.$implicit; const ctx_r11 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r11.select("umaszczenie", element_r10[0].value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element_r10 = ctx.$implicit;
    const ctx_r2 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r2.selectClass("umaszczenie", element_r10[0].value));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r10[1].value);
} }
function FiltersComponent_button_19_Template(rf, ctx) { if (rf & 1) {
    const _r15 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 5);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FiltersComponent_button_19_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r15); const element_r13 = ctx.$implicit; const ctx_r14 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r14.select("siersc", element_r13[0].value); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element_r13 = ctx.$implicit;
    const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r3.selectClass("siersc", element_r13[0].value));
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r13[1].value);
} }
class FiltersComponent {
    constructor(crudService, filtersService) {
        this.crudService = crudService;
        this.filtersService = filtersService;
        this.Rozmiar = [];
        this.Siersc = [];
        this.Charakter = [];
        this.Umaszczenie = [];
        this.selectedRozmiar = [];
        this.selectedSiersc = [];
        this.selectedCharakter = [];
        this.selectedUmaszczenie = [];
        this.selected = [];
        this.RasyRozmiar = [];
        this.RasySiersc = [];
        this.RasyCharakter = [];
        this.RasyUmaszczenie = [];
        this.filteredRozmiar = [];
        this.filteredSiersc = [];
        this.filteredCharakter = [];
        this.filteredUmaszczenie = [];
        this.filteredRasy = [];
        this.data = new _angular_core__WEBPACK_IMPORTED_MODULE_0__["EventEmitter"]();
    }
    ngOnInit() {
        this.crudService.GetAllRozmiar().subscribe(res => {
            this.Rozmiar = res;
            for (let i = 0; i < res.length; i++) {
                this.selectedRozmiar.push({ id: res[i][0].value, selected: false });
            }
        });
        this.crudService.GetAllSiersc().subscribe(res => {
            this.Siersc = res;
            for (let i = 0; i < res.length; i++) {
                this.selectedSiersc.push({ id: res[i][0].value, selected: false });
            }
        });
        this.crudService.GetAllCharakter().subscribe(res => {
            this.Charakter = res;
            for (let i = 0; i < res.length; i++) {
                this.selectedCharakter.push({ id: res[i][0].value, selected: false });
            }
        });
        this.crudService.GetAllUmaszczenie().subscribe(res => {
            this.Umaszczenie = res;
            for (let i = 0; i < res.length; i++) {
                this.selectedUmaszczenie.push({ id: res[i][0].value, selected: false });
            }
        });
        this.crudService.GetFilterUmaszczenie().subscribe(res => {
            for (let i = 0; i < res.length; i++)
                this.RasyUmaszczenie.push({ rasa: res[i][1].value, id: res[i][0].value });
        });
        this.crudService.GetFilterSiersc().subscribe(res => {
            for (let i = 0; i < res.length; i++)
                this.RasySiersc.push({ rasa: res[i][1].value, id: res[i][0].value });
        });
        this.crudService.GetFilterCharakter().subscribe(res => {
            for (let i = 0; i < res.length; i++)
                this.RasyCharakter.push({ rasa: res[i][1].value, id: res[i][0].value });
        });
        this.crudService.GetFilterRozmiar().subscribe(res => {
            for (let i = 0; i < res.length; i++)
                this.RasyRozmiar.push({ rasa: res[i][1].value, id: res[i][0].value });
        });
    }
    select(type, id) {
        switch (type) {
            case 'rozmiar': {
                let index = this.selectedRozmiar.findIndex(x => x.id == id);
                this.selectedRozmiar[index].selected = !this.selectedRozmiar[index].selected;
                break;
            }
            case 'siersc': {
                let index = this.selectedSiersc.findIndex(x => x.id == id);
                this.selectedSiersc[index].selected = !this.selectedSiersc[index].selected;
                break;
            }
            case 'charakter': {
                let index = this.selectedCharakter.findIndex(x => x.id == id);
                this.selectedCharakter[index].selected = !this.selectedCharakter[index].selected;
                break;
            }
            case 'umaszczenie': {
                let index = this.selectedUmaszczenie.findIndex(x => x.id == id);
                this.selectedUmaszczenie[index].selected = !this.selectedUmaszczenie[index].selected;
                break;
            }
        }
    }
    selectClass(type, id) {
        switch (type) {
            case 'rozmiar':
                return (this.selectedRozmiar[this.selectedRozmiar.findIndex(x => x.id == id)].selected == true) ? 'selected' : 'unselected';
            case 'siersc':
                return (this.selectedSiersc[this.selectedSiersc.findIndex(x => x.id == id)].selected == true) ? 'selected' : 'unselected';
            case 'charakter':
                return (this.selectedCharakter[this.selectedCharakter.findIndex(x => x.id == id)].selected) ? 'selected' : 'unselected';
            case 'umaszczenie':
                return (this.selectedUmaszczenie[this.selectedUmaszczenie.findIndex(x => x.id == id)].selected) ? 'selected' : 'unselected';
        }
    }
    clear() {
        this.selectedRozmiar.forEach(x => x.selected = false);
        this.selectedCharakter.forEach(x => x.selected = false);
        this.selectedUmaszczenie.forEach(x => x.selected = false);
        this.selectedSiersc.forEach(x => x.selected = false);
        this.filteredRasy = [];
        this.filteredRasy.push(true);
        this.data.emit(this.filteredRasy);
    }
    filter() {
        this.selectedCount = 0;
        this.selected = [];
        let rozmiar = this.filtersService.findInArray(this.selectedRozmiar, this.RasyRozmiar);
        this.selectedCount = +rozmiar.selectedCount;
        this.selected['rozmiar'] = rozmiar.selected;
        this.filteredRozmiar = rozmiar.filtered;
        let charakter = this.filtersService.findInArray(this.selectedCharakter, this.RasyCharakter);
        this.selectedCount = +charakter.selectedCount;
        this.selected['charakter'] = charakter.selected;
        this.filteredCharakter = charakter.filtered;
        let umaszczenie = this.filtersService.findInArray(this.selectedUmaszczenie, this.RasyUmaszczenie);
        this.selectedCount = +umaszczenie.selectedCount;
        this.selected['umaszczenie'] = umaszczenie.selected;
        this.filteredUmaszczenie = umaszczenie.filtered;
        let siersc = this.filtersService.findInArray(this.selectedSiersc, this.RasySiersc);
        this.selectedCount = +siersc.selectedCount;
        this.selected['siersc'] = siersc.selected;
        this.filteredSiersc = siersc.filtered;
        this.filteredRasy = [];
        this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredRozmiar, this.selected['rozmiar']);
        this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredCharakter, this.selected['charakter']);
        this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredUmaszczenie, this.selected['umaszczenie']);
        this.filteredRasy = this.filtersService.andFilters(this.filteredRasy, this.filteredSiersc, this.selected['siersc']);
        if (this.filteredRasy.length == 0) {
            if (this.selectedCount == 0)
                this.filteredRasy.push(true);
            else
                this.filteredRasy.push(false);
        }
        this.data.emit(this.filteredRasy);
    }
}
FiltersComponent.ɵfac = function FiltersComponent_Factory(t) { return new (t || FiltersComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_crud_service__WEBPACK_IMPORTED_MODULE_1__["CrudService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_filters_service__WEBPACK_IMPORTED_MODULE_2__["FiltersService"])); };
FiltersComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: FiltersComponent, selectors: [["app-filters"]], outputs: { data: "data" }, decls: 22, vars: 4, consts: [["id", "clear"], [1, "unselected", 3, "click"], [1, "items"], [3, "ngClass", "click", 4, "ngFor", "ngForOf"], ["id", "search", 3, "click"], [3, "ngClass", "click"]], template: function FiltersComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "aside");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "button", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FiltersComponent_Template_button_click_2_listener() { return ctx.clear(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](3, "Wyczy\u015B\u0107");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](4, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](6, "Rozmiary");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](7, FiltersComponent_button_7_Template, 2, 2, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](8, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](10, "Charakter i zachowanie");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](11, FiltersComponent_button_11_Template, 2, 2, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](12, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](13, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](14, "Umaszczenie");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](15, FiltersComponent_button_15_Template, 2, 2, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](16, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](17, "h3");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](18, "Rodzaj sier\u015Bci");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](19, FiltersComponent_button_19_Template, 2, 2, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](20, "button", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function FiltersComponent_Template_button_click_20_listener() { return ctx.filter(); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](21, "SZUKAJ");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.Rozmiar);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.Charakter);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.Umaszczenie);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.Siersc);
    } }, directives: [_angular_common__WEBPACK_IMPORTED_MODULE_3__["NgForOf"], _angular_common__WEBPACK_IMPORTED_MODULE_3__["NgClass"]], styles: ["aside[_ngcontent-%COMP%] {\n  color: #555555;\n  background-color: #f2f2f2;\n  padding: 2rem;\n}\n\n#clear[_ngcontent-%COMP%] {\n  text-align: right;\n  margin-bottom: 2rem;\n}\n\nh3[_ngcontent-%COMP%] {\n  font-weight: 700;\n}\n\n.items[_ngcontent-%COMP%] {\n  margin-bottom: 2rem;\n}\n\nbutton[_ngcontent-%COMP%] {\n  font-weight: 600;\n  padding: 0.4rem;\n  margin: 0.3rem;\n}\n\nbutton[_ngcontent-%COMP%]:hover, .selected[_ngcontent-%COMP%] {\n  background-color: #cda398;\n  border: #cda398 1px solid;\n}\n\n#clear[_ngcontent-%COMP%]   button[_ngcontent-%COMP%]:hover, .unselected[_ngcontent-%COMP%] {\n  border: 1px solid #555555;\n  color: #555555;\n  background-color: #f2f2f2;\n}\n\n#search[_ngcontent-%COMP%] {\n  width: 100%;\n  border: none;\n  color: white;\n  font-weight: 1000;\n  font-size: 1.25rem;\n  background-color: #cda398;\n}\n\n@media (min-width: 768px) {\n  aside[_ngcontent-%COMP%] {\n    position: fixed;\n    left: 0;\n    width: 25%;\n    height: 100vh;\n    overflow-y: auto;\n    padding: 0.3rem 0.3rem 12rem 0.3rem;\n  }\n\n  #search[_ngcontent-%COMP%] {\n    margin: 0;\n  }\n}\n\n@media (min-width: 1100px) {\n  button[_ngcontent-%COMP%] {\n    padding: 0.4rem 1rem;\n  }\n\n  aside[_ngcontent-%COMP%] {\n    width: 20%;\n    padding: 2rem 2rem 12rem 2rem;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGZpbHRlcnMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxjQUFBO0VBQ0EseUJBQUE7RUFDQSxhQUFBO0FBQ0o7O0FBRUE7RUFDSSxpQkFBQTtFQUNBLG1CQUFBO0FBQ0o7O0FBRUE7RUFDSSxnQkFBQTtBQUNKOztBQUVBO0VBQ0ksbUJBQUE7QUFDSjs7QUFFQTtFQUNJLGdCQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0VBQ0EsWUFBQTtFQUNBLFlBQUE7RUFDQSxpQkFBQTtFQUNBLGtCQUFBO0VBQ0EseUJBQUE7QUFDSjs7QUFFQTtFQUNJO0lBQ0ksZUFBQTtJQUNBLE9BQUE7SUFDQSxVQUFBO0lBQ0EsYUFBQTtJQUNBLGdCQUFBO0lBQ0EsbUNBQUE7RUFDTjs7RUFHRTtJQUFVLFNBQUE7RUFDWjtBQUNGOztBQUVBO0VBRUk7SUFDSSxvQkFBQTtFQUROOztFQUdFO0lBQ0ksVUFBQTtJQUNBLDZCQUFBO0VBQU47QUFDRiIsImZpbGUiOiJmaWx0ZXJzLmNvbXBvbmVudC5zY3NzIiwic291cmNlc0NvbnRlbnQiOlsiYXNpZGUge1xyXG4gICAgY29sb3I6ICM1NTU1NTU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjZjJmMmYyO1xyXG4gICAgcGFkZGluZzogMnJlbVxyXG59XHJcblxyXG4jY2xlYXIge1xyXG4gICAgdGV4dC1hbGlnbjogcmlnaHQ7XHJcbiAgICBtYXJnaW4tYm90dG9tOiAycmVtO1xyXG59XHJcblxyXG5oMyB7XHJcbiAgICBmb250LXdlaWdodDogNzAwO1xyXG59XHJcblxyXG4uaXRlbXMge1xyXG4gICAgbWFyZ2luLWJvdHRvbTogMnJlbTtcclxufVxyXG5cclxuYnV0dG9uIHtcclxuICAgIGZvbnQtd2VpZ2h0OiA2MDA7XHJcbiAgICBwYWRkaW5nOiAwLjRyZW07XHJcbiAgICBtYXJnaW46IDAuM3JlbTtcclxufVxyXG5cclxuYnV0dG9uOmhvdmVyLCAuc2VsZWN0ZWR7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2RhMzk4O1xyXG4gICAgYm9yZGVyOiAjY2RhMzk4IDFweCBzb2xpZDtcclxufVxyXG5cclxuI2NsZWFyIGJ1dHRvbjpob3ZlciwgLnVuc2VsZWN0ZWQge1xyXG4gICAgYm9yZGVyOiAxcHggc29saWQgIzU1NTU1NTtcclxuICAgIGNvbG9yOiAjNTU1NTU1O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2YyZjJmMjtcclxufVxyXG5cclxuI3NlYXJjaCB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxuICAgIGJvcmRlcjogbm9uZTtcclxuICAgIGNvbG9yOiB3aGl0ZTtcclxuICAgIGZvbnQtd2VpZ2h0OiAxMDAwO1xyXG4gICAgZm9udC1zaXplOiAxLjI1cmVtO1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogI2NkYTM5ODtcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDc2OHB4KSB7XHJcbiAgICBhc2lkZSB7XHJcbiAgICAgICAgcG9zaXRpb246IGZpeGVkO1xyXG4gICAgICAgIGxlZnQ6IDA7XHJcbiAgICAgICAgd2lkdGg6IDI1JTtcclxuICAgICAgICBoZWlnaHQ6IDEwMHZoO1xyXG4gICAgICAgIG92ZXJmbG93LXk6IGF1dG87XHJcbiAgICAgICAgcGFkZGluZzogMC4zcmVtIDAuM3JlbSAxMnJlbSAwLjNyZW07XHJcblxyXG4gICAgfVxyXG5cclxuICAgICNzZWFyY2ggeyBtYXJnaW46IDB9XHJcbiAgICBcclxufVxyXG5cclxuQG1lZGlhIChtaW4td2lkdGg6IDExMDBweCkge1xyXG5cclxuICAgIGJ1dHRvbiB7XHJcbiAgICAgICAgcGFkZGluZzogMC40cmVtIDFyZW07XHJcbiAgICB9XHJcbiAgICBhc2lkZSB7XHJcbiAgICAgICAgd2lkdGg6IDIwJTtcclxuICAgICAgICBwYWRkaW5nOiAycmVtIDJyZW0gMTJyZW0gMnJlbTtcclxuICAgIH1cclxufSJdfQ== */"] });


/***/ }),

/***/ "v69q":
/*!****************************************************!*\
  !*** ./src/app/all-breeds/all-breeds.component.ts ***!
  \****************************************************/
/*! exports provided: AllBreedsComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AllBreedsComponent", function() { return AllBreedsComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _service_crud_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../service/crud.service */ "+JMX");
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _filters_filters_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../filters/filters.component */ "sH3A");
/* harmony import */ var _angular_common__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! @angular/common */ "ofXK");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! @angular/forms */ "3Pt+");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! @angular/router */ "tyNb");







function AllBreedsComponent_button_4_Template(rf, ctx) { if (rf & 1) {
    const _r4 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵgetCurrentView"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "button", 8);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("click", function AllBreedsComponent_button_4_Template_button_click_0_listener() { _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵrestoreView"](_r4); const element_r2 = ctx.$implicit; const ctx_r3 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"](); return ctx_r3.alphabetSearch(element_r2.letter); });
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element_r2 = ctx.$implicit;
    const ctx_r0 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngClass", ctx_r0.selectedLetter == element_r2.letter ? "selected" : "unselected")("disabled", element_r2.state == false);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r2.letter);
} }
function AllBreedsComponent_div_10_ng_container_2_img_1_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelement"](0, "img", 13);
} if (rf & 2) {
    const element2_r7 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("src", element2_r7[1].value, _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵsanitizeUrl"]);
} }
function AllBreedsComponent_div_10_ng_container_2_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerStart"](0);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](1, AllBreedsComponent_div_10_ng_container_2_img_1_Template, 1, 1, "img", 12);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementContainerEnd"]();
} if (rf & 2) {
    const element2_r7 = ctx.$implicit;
    const element_r5 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]().$implicit;
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngIf", element2_r7[0].value == element_r5[1].value);
} }
function AllBreedsComponent_div_10_Template(rf, ctx) { if (rf & 1) {
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "div", 9);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 10);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](2, AllBreedsComponent_div_10_ng_container_2_Template, 2, 1, "ng-container", 11);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "h4");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](4);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
} if (rf & 2) {
    const element_r5 = ctx.$implicit;
    const ctx_r1 = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵnextContext"]();
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵpropertyInterpolate1"]("routerLink", "/breeds/", element_r5[1].value, "");
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx_r1.Zdjecia);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
    _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](element_r5[0].value);
} }
class AllBreedsComponent {
    constructor(crudService, sanitizer) {
        this.crudService = crudService;
        this.sanitizer = sanitizer;
        this.Rasy = [];
        this.Zdjecia = [];
        this.Alfabet = [{ letter: 'A', state: false }, { letter: 'B', state: false }, { letter: 'C', state: false },
            { letter: 'D', state: false }, { letter: 'E', state: false }, { letter: 'F', state: false },
            { letter: 'G', state: false }, { letter: 'H', state: false }, { letter: 'I', state: false },
            { letter: 'J', state: false }, { letter: 'K', state: false }, { letter: 'L', state: false },
            { letter: 'M', state: false }, { letter: 'N', state: false }, { letter: 'O', state: false },
            { letter: 'P', state: false }, { letter: 'Q', state: false }, { letter: 'R', state: false },
            { letter: 'S', state: false }, { letter: 'T', state: false }, { letter: 'U', state: false },
            { letter: 'V', state: false }, { letter: 'W', state: false }, { letter: 'X', state: false },
            { letter: 'Y', state: false }, { letter: 'Z', state: false }, { letter: 'WSZYSTKIE RASY', state: true }];
        this.displayedRasy = [];
        this.filteredRasy = [];
        this.searchedRasy = [];
    }
    ngOnInit() {
        this.crudService.GetRasy().subscribe(res => {
            this.Rasy = res;
            this.displayedRasy = res;
            this.searchedRasy = res;
            this.filteredRasy = res;
            for (let i = 0; i < res.length; i++) {
                let letter = res[i][0].value.substring(0, 1).toUpperCase();
                let index = this.Alfabet.findIndex(x => x.letter == letter);
                this.Alfabet[index].state = true;
            }
        });
        this.crudService.GetZdjecia().subscribe(res => {
            this.Zdjecia = res;
            for (var i = 0; i < this.Zdjecia.length; i++) {
                var binary = '';
                var bytes = new Uint8Array(this.Zdjecia[i][1].value.data);
                var len = bytes.byteLength;
                for (var j = 0; j < len; j++) {
                    binary += String.fromCharCode(bytes[j]);
                }
                var btoaVal = btoa(binary);
                this.Zdjecia[i][1].value = this.sanitizer.bypassSecurityTrustResourceUrl('data:image/jpg;base64,' + btoaVal);
            }
        });
        this.selectedLetter = 'WSZYSTKIE RASY';
    }
    search(event) {
        if (this.searchedRasy.length > 0) {
            this.filterInfo = '';
            let filtered = this.searchedRasy;
            if (event.target.value) {
                let text = event.target.value.toLowerCase();
                filtered = filtered.filter(i => i[0].value.toLowerCase().includes(text));
            }
            if (filtered.length == 0) {
                this.filterInfo = 'Nie znaleziono takiej rasy. Przepraszamy.';
            }
            this.displayedRasy = filtered;
        }
    }
    alphabetSearch(letter) {
        this.filterInfo = '';
        this.selectedLetter = letter;
        this.searchName = '';
        if (letter == 'WSZYSTKIE RASY') {
            this.displayedRasy = this.filteredRasy;
            this.searchedRasy = this.filteredRasy;
        }
        else {
            let filtered = this.filteredRasy;
            filtered = filtered.filter(i => i[0].value.toUpperCase().substring(0, 1).includes(letter));
            this.displayedRasy = filtered;
            this.searchedRasy = filtered;
            if (filtered == 0)
                this.filterInfo = 'Brak ras na wybraną literę. Przepraszamy.';
        }
    }
    eventHandler(event) {
        this.filterInfo = '';
        this.searchName = '';
        if (event[0] === true)
            this.filteredRasy = this.Rasy;
        else if (event[0] === false) {
            this.filterInfo = 'Brak ras o wybranych cechach. Przepraszamy.';
            this.filteredRasy = [];
        }
        else {
            let filtered = [];
            for (let i = 0; i < this.Rasy.length; i++) {
                if (event.includes(this.Rasy[i][1].value)) {
                    filtered.push(this.Rasy[i]);
                }
            }
            this.filteredRasy = filtered;
        }
        this.displayedRasy = this.filteredRasy;
    }
}
AllBreedsComponent.ɵfac = function AllBreedsComponent_Factory(t) { return new (t || AllBreedsComponent)(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_service_crud_service__WEBPACK_IMPORTED_MODULE_1__["CrudService"]), _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdirectiveInject"](_angular_platform_browser__WEBPACK_IMPORTED_MODULE_2__["DomSanitizer"])); };
AllBreedsComponent.ɵcmp = _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵdefineComponent"]({ type: AllBreedsComponent, selectors: [["app-all-breeds"]], decls: 11, vars: 4, consts: [[3, "data"], ["id", "container"], ["id", "search"], [3, "ngClass", "disabled", "click", 4, "ngFor", "ngForOf"], ["type", "text", "placeholder", "Szukaj rasy...", 3, "ngModel", "keyup", "ngModelChange"], ["id", "info"], ["id", "breeds"], ["class", "dog", 3, "routerLink", 4, "ngFor", "ngForOf"], [3, "ngClass", "disabled", "click"], [1, "dog", 3, "routerLink"], [1, "image"], [4, "ngFor", "ngForOf"], [3, "src", 4, "ngIf"], [3, "src"]], template: function AllBreedsComponent_Template(rf, ctx) { if (rf & 1) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](0, "app-filters", 0);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("data", function AllBreedsComponent_Template_app_filters_data_0_listener($event) { return ctx.eventHandler($event); });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](1, "div", 1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](2, "div", 2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](3, "div");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](4, AllBreedsComponent_button_4_Template, 2, 3, "button", 3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](5, "input", 4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵlistener"]("keyup", function AllBreedsComponent_Template_input_keyup_5_listener($event) { return ctx.search($event); })("ngModelChange", function AllBreedsComponent_Template_input_ngModelChange_5_listener($event) { return ctx.searchName = $event; });
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](6, "div", 5);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](7, "h4");
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtext"](8);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementStart"](9, "div", 6);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtemplate"](10, AllBreedsComponent_div_10_Template, 5, 3, "div", 7);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵelementEnd"]();
    } if (rf & 2) {
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](4);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.Alfabet);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](1);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngModel", ctx.searchName);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](3);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵtextInterpolate"](ctx.filterInfo);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵadvance"](2);
        _angular_core__WEBPACK_IMPORTED_MODULE_0__["ɵɵproperty"]("ngForOf", ctx.displayedRasy);
    } }, directives: [_filters_filters_component__WEBPACK_IMPORTED_MODULE_3__["FiltersComponent"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgForOf"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["DefaultValueAccessor"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgControlStatus"], _angular_forms__WEBPACK_IMPORTED_MODULE_5__["NgModel"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgClass"], _angular_router__WEBPACK_IMPORTED_MODULE_6__["RouterLink"], _angular_common__WEBPACK_IMPORTED_MODULE_4__["NgIf"]], styles: ["#container[_ngcontent-%COMP%] {\n  padding: 1.5rem 3rem;\n  position: relative;\n}\n\n#search[_ngcontent-%COMP%] {\n  padding: 2rem 0;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  text-align: center;\n}\n\nbutton[_ngcontent-%COMP%] {\n  font-weight: 600;\n  padding: 0.3rem 0.5rem;\n  min-width: 2.1rem;\n  margin: 0.35rem;\n  border: 1px solid #555555;\n  color: #555555;\n  background-color: white;\n}\n\nbutton[_ngcontent-%COMP%]:hover, .selected[_ngcontent-%COMP%] {\n  background-color: #cda398;\n  border: #cda398 1px solid;\n}\n\nbutton[_ngcontent-%COMP%]:disabled {\n  background-color: lightgray;\n  color: slategray;\n}\n\nbutton[_ngcontent-%COMP%]:disabled:hover {\n  border: 1px solid #555555;\n  background-color: lightgray;\n}\n\n.unselected[_ngcontent-%COMP%] {\n  border: 1px solid #555555;\n  background-color: white;\n}\n\ninput[_ngcontent-%COMP%] {\n  width: 85%;\n  padding: 0.6rem;\n  margin: 3rem 0;\n}\n\n#info[_ngcontent-%COMP%] {\n  text-align: center;\n}\n\n#breeds[_ngcontent-%COMP%] {\n  display: flex;\n  flex-direction: column;\n}\n\n.dog[_ngcontent-%COMP%] {\n  margin: 3rem 0;\n  display: flex;\n  flex-direction: column;\n  align-items: center;\n  justify-content: center;\n}\n\nimg[_ngcontent-%COMP%] {\n  width: 100%;\n}\n\n.image[_ngcontent-%COMP%] {\n  width: 80%;\n  position: relative;\n  background-color: white;\n}\n\n.image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]:nth-child(1) {\n  transition: opacity 1s;\n}\n\n.image[_ngcontent-%COMP%]   img[_ngcontent-%COMP%]:nth-child(2) {\n  position: absolute;\n  left: 0;\n  object-fit: contain;\n  opacity: 0;\n  transition: opacity 3s;\n  max-height: 100%;\n  max-width: 100%;\n}\n\n.image[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%]:nth-child(2) {\n  opacity: 1;\n}\n\n.image[_ngcontent-%COMP%]:hover   img[_ngcontent-%COMP%]:nth-child(1) {\n  opacity: 0;\n}\n\n@media (min-width: 768px) {\n  #container[_ngcontent-%COMP%] {\n    position: relative;\n    margin-left: 25%;\n  }\n\n  #breeds[_ngcontent-%COMP%] {\n    flex-wrap: wrap;\n    flex-direction: row;\n  }\n\n  .dog[_ngcontent-%COMP%] {\n    width: 50%;\n  }\n}\n\n@media (min-width: 1100px) {\n  #container[_ngcontent-%COMP%] {\n    margin-left: 20%;\n  }\n\n  .dog[_ngcontent-%COMP%] {\n    width: 33%;\n  }\n}\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uXFwuLlxcLi5cXGFsbC1icmVlZHMuY29tcG9uZW50LnNjc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDSSxvQkFBQTtFQUNBLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxlQUFBO0VBQ0EsYUFBQTtFQUNBLHNCQUFBO0VBQ0EsbUJBQUE7RUFDQSxrQkFBQTtBQUNKOztBQUVBO0VBQ0ksZ0JBQUE7RUFDQSxzQkFBQTtFQUNBLGlCQUFBO0VBQ0EsZUFBQTtFQUNBLHlCQUFBO0VBQ0EsY0FBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLHlCQUFBO0FBQ0o7O0FBRUE7RUFDSSwyQkFBQTtFQUNBLGdCQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLDJCQUFBO0FBQ0o7O0FBRUE7RUFDSSx5QkFBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0EsZUFBQTtFQUNBLGNBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0FBQ0o7O0FBRUE7RUFDSSxhQUFBO0VBQ0Esc0JBQUE7QUFDSjs7QUFFQTtFQUNJLGNBQUE7RUFDQSxhQUFBO0VBQ0Esc0JBQUE7RUFDQSxtQkFBQTtFQUNBLHVCQUFBO0FBQ0o7O0FBRUE7RUFDSSxXQUFBO0FBQ0o7O0FBRUE7RUFDSSxVQUFBO0VBQ0Esa0JBQUE7RUFDQSx1QkFBQTtBQUNKOztBQUVBO0VBQ0ksc0JBQUE7QUFDSjs7QUFFQTtFQUNJLGtCQUFBO0VBQ0EsT0FBQTtFQUNBLG1CQUFBO0VBQ0EsVUFBQTtFQUNBLHNCQUFBO0VBQ0EsZ0JBQUE7RUFDQSxlQUFBO0FBQ0o7O0FBR0s7RUFBa0IsVUFBQTtBQUN2Qjs7QUFBSztFQUFrQixVQUFBO0FBR3ZCOztBQUFBO0VBQ0k7SUFDSSxrQkFBQTtJQUNBLGdCQUFBO0VBR047O0VBQUU7SUFDSSxlQUFBO0lBQ0EsbUJBQUE7RUFHTjs7RUFBRTtJQUNJLFVBQUE7RUFHTjtBQUNGOztBQUFBO0VBQ0k7SUFDSSxnQkFBQTtFQUVOOztFQUFFO0lBQ0ksVUFBQTtFQUdOO0FBQ0YiLCJmaWxlIjoiYWxsLWJyZWVkcy5jb21wb25lbnQuc2NzcyIsInNvdXJjZXNDb250ZW50IjpbIiNjb250YWluZXIge1xyXG4gICAgcGFkZGluZzogMS41cmVtIDNyZW07XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbn1cclxuXHJcbiNzZWFyY2gge1xyXG4gICAgcGFkZGluZzogMnJlbSAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAgdGV4dC1hbGlnbjogY2VudGVyO1xyXG59XHJcblxyXG5idXR0b24ge1xyXG4gICAgZm9udC13ZWlnaHQ6IDYwMDtcclxuICAgIHBhZGRpbmc6IDAuM3JlbSAwLjVyZW07XHJcbiAgICBtaW4td2lkdGg6IDIuMXJlbTtcclxuICAgIG1hcmdpbjogMC4zNXJlbTtcclxuICAgIGJvcmRlcjogMXB4IHNvbGlkICM1NTU1NTU7XHJcbiAgICBjb2xvcjogIzU1NTU1NTtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IHdoaXRlO1xyXG59XHJcblxyXG5idXR0b246aG92ZXIsIC5zZWxlY3RlZCB7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiAjY2RhMzk4O1xyXG4gICAgYm9yZGVyOiAjY2RhMzk4IDFweCBzb2xpZDtcclxufVxyXG5cclxuYnV0dG9uOmRpc2FibGVkIHtcclxuICAgIGJhY2tncm91bmQtY29sb3I6IGxpZ2h0Z3JheTtcclxuICAgIGNvbG9yOiBzbGF0ZWdyYXk7XHJcbn1cclxuXHJcbmJ1dHRvbjpkaXNhYmxlZDpob3ZlciB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNTU1NTU1O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogbGlnaHRncmF5O1xyXG59XHJcblxyXG4udW5zZWxlY3RlZCB7XHJcbiAgICBib3JkZXI6IDFweCBzb2xpZCAjNTU1NTU1O1xyXG4gICAgYmFja2dyb3VuZC1jb2xvcjogd2hpdGU7XHJcbn1cclxuXHJcbmlucHV0e1xyXG4gICAgd2lkdGg6IDg1JTtcclxuICAgIHBhZGRpbmc6IDAuNnJlbTtcclxuICAgIG1hcmdpbjogM3JlbSAwO1xyXG59XHJcblxyXG4jaW5mb3tcclxuICAgIHRleHQtYWxpZ246IGNlbnRlcjtcclxufVxyXG5cclxuI2JyZWVkc3tcclxuICAgIGRpc3BsYXk6IGZsZXg7XHJcbiAgICBmbGV4LWRpcmVjdGlvbjogY29sdW1uO1xyXG59XHJcblxyXG4uZG9nIHtcclxuICAgIG1hcmdpbjogM3JlbSAwO1xyXG4gICAgZGlzcGxheTogZmxleDtcclxuICAgIGZsZXgtZGlyZWN0aW9uOiBjb2x1bW47XHJcbiAgICBhbGlnbi1pdGVtczogY2VudGVyO1xyXG4gICAganVzdGlmeS1jb250ZW50OiBjZW50ZXI7XHJcbn1cclxuXHJcbmltZyB7XHJcbiAgICB3aWR0aDogMTAwJTtcclxufVxyXG5cclxuLmltYWdlIHtcclxuICAgIHdpZHRoOiA4MCU7XHJcbiAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICBiYWNrZ3JvdW5kLWNvbG9yOiB3aGl0ZTtcclxufVxyXG5cclxuLmltYWdlIGltZzpudGgtY2hpbGQoMSkge1xyXG4gICAgdHJhbnNpdGlvbjogb3BhY2l0eSAxcztcclxufVxyXG5cclxuLmltYWdlIGltZzpudGgtY2hpbGQoMikge1xyXG4gICAgcG9zaXRpb246IGFic29sdXRlO1xyXG4gICAgbGVmdDogMDsgXHJcbiAgICBvYmplY3QtZml0OiBjb250YWluO1xyXG4gICAgb3BhY2l0eTogMDtcclxuICAgIHRyYW5zaXRpb246IG9wYWNpdHkgM3M7XHJcbiAgICBtYXgtaGVpZ2h0OiAxMDAlO1xyXG4gICAgbWF4LXdpZHRoOiAxMDAlO1xyXG59XHJcblxyXG4uaW1hZ2U6aG92ZXJ7XHJcbiAgICAgaW1nOm50aC1jaGlsZCgyKSB7b3BhY2l0eTogMTt9XHJcbiAgICAgaW1nOm50aC1jaGlsZCgxKSB7b3BhY2l0eTogMDt9XHJcbn1cclxuXHJcbkBtZWRpYSAobWluLXdpZHRoOiA3NjhweCkge1xyXG4gICAgI2NvbnRhaW5lcntcclxuICAgICAgICBwb3NpdGlvbjogcmVsYXRpdmU7XHJcbiAgICAgICAgbWFyZ2luLWxlZnQ6IDI1JTtcclxuICAgIH1cclxuXHJcbiAgICAjYnJlZWRze1xyXG4gICAgICAgIGZsZXgtd3JhcDogd3JhcDtcclxuICAgICAgICBmbGV4LWRpcmVjdGlvbjogcm93O1xyXG4gICAgfVxyXG5cclxuICAgIC5kb2cge1xyXG4gICAgICAgIHdpZHRoOiA1MCU7XHJcbiAgICB9XHJcbiAgICBcclxufVxyXG5AbWVkaWEgKG1pbi13aWR0aDogMTEwMHB4KSB7XHJcbiAgICAjY29udGFpbmVye1xyXG4gICAgICAgIG1hcmdpbi1sZWZ0OiAyMCU7XHJcbiAgICB9XHJcbiAgICAuZG9ne1xyXG4gICAgICAgIHdpZHRoOiAzMyU7XHJcbiAgICB9XHJcbiAgICBcclxufSJdfQ== */"] });


/***/ }),

/***/ "vY5A":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "tyNb");
/* harmony import */ var _all_breeds_all_breeds_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./all-breeds/all-breeds.component */ "v69q");
/* harmony import */ var _dog_detail_dog_detail_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./dog-detail/dog-detail.component */ "1xP8");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! @angular/core */ "fXoL");





const routes = [
    { path: '', pathMatch: 'full', redirectTo: 'breeds' },
    { path: 'breeds', component: _all_breeds_all_breeds_component__WEBPACK_IMPORTED_MODULE_1__["AllBreedsComponent"] },
    { path: 'breeds/:id', component: _dog_detail_dog_detail_component__WEBPACK_IMPORTED_MODULE_2__["DogDetailComponent"] }
];
class AppRoutingModule {
}
AppRoutingModule.ɵfac = function AppRoutingModule_Factory(t) { return new (t || AppRoutingModule)(); };
AppRoutingModule.ɵmod = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineNgModule"]({ type: AppRoutingModule });
AppRoutingModule.ɵinj = _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵdefineInjector"]({ imports: [[_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"].forRoot(routes)], _angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] });
(function () { (typeof ngJitMode === "undefined" || ngJitMode) && _angular_core__WEBPACK_IMPORTED_MODULE_3__["ɵɵsetNgModuleScope"](AppRoutingModule, { imports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]], exports: [_angular_router__WEBPACK_IMPORTED_MODULE_0__["RouterModule"]] }); })();


/***/ }),

/***/ "zUnb":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "jhN1");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "fXoL");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "ZAI4");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "AytR");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["enableProdMode"])();
}
_angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["platformBrowser"]().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(err => console.error(err));


/***/ }),

/***/ "zn8P":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "zn8P";

/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map